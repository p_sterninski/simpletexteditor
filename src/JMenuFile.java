

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * Obiekt JMenuFile tworzy podmenu "File" z opcjami: Open, Save, Save ass..., Exit.
 * Klasa impementuje również nasłuch do tych opcji.
 * @author Przemysław Sterniński
 *
 */
class JMenuFile extends JMenuAbstract {
	
	public JMenuFile(EditorView widok){
		super("File");
		
		utworzElementMenu(this, "Open", null, 'd', "control O", null);
		utworzElementMenu(this, "Save", null, 's', "control S", null);
		utworzElementMenu(this, "Save as...", null, 'e', "control E", "Save");
		utworzElementMenu(this, null, new IconLinia(Color.RED), 0, null, null); //separator
		utworzElementMenu(this, "Exit", null, 't', "control T", null);
		
		
		ActionListener nasluchWyboruPliku = new ActionListener() {
			
			private File katalog = new File(".");
			private File plik;
			private JFileChooser wyborPliku = new JFileChooser(new File("."));
			{
			wyborPliku.setFileFilter(new FileNameExtensionFilter(".txt Files", "txt"));
			}
			
			public void actionPerformed(ActionEvent e) {
				String actionCmd = e.getActionCommand();
				
				if(actionCmd.equals("Exit")) {
					System.exit(0);
				
				}else if (((AbstractButton)e.getSource()).getText().equals("Save") && plik != null) { //po wyborze Save jesli wczesniej byl wybrany plik nie pokaze sie okno dialogowe
					try(BufferedWriter bw = new BufferedWriter(new FileWriter(plik))){
						widok.getObszarTxt().write(bw);
					
					} catch (IOException ex0) {
						System.out.println("Błąd przy zapisie pliku");
						return;
					}
				
				}else if (actionCmd.equals("Open") || actionCmd.equals("Save")){
					int retVal = wyborPliku.showDialog(null, actionCmd);
	                if (retVal == JFileChooser.APPROVE_OPTION){
						plik = wyborPliku.getSelectedFile();
						katalog = plik.getParentFile();
	                
		                if (actionCmd.equals("Open")){
		                	try (BufferedReader br = new BufferedReader(new FileReader(plik))){
								widok.getObszarTxt().setText(null); //wyczyszczenie obszaru tekstowego przed wczytaniem zawartosci nowego pliku
								String liniaTekstu = "";
								while((liniaTekstu = br.readLine())!=null)
									widok.getObszarTxt().append(liniaTekstu + "\n");
							
		                	}catch (FileNotFoundException ex1) {
								System.out.println("Błąd przy otwieraniu pliku");
								return;
							
		                	}catch (IOException ex2) {
								System.out.println("Błąd przy wczytywaniu tekstu z pliku");
								return;
							}	
		                }
		                
		                if (actionCmd.equals("Save")){
		                	try(BufferedWriter bw = new BufferedWriter(new FileWriter(plik))){
		                		widok.getObszarTxt().write(bw);
		                	
		                	}catch (IOException ex3) {
								System.out.println("Błąd przy zapisie pliku");
								return;
							}
		                }
	                
		                widok.setTitle(widok.getNazwaProgramu() + plik.toString());
	                }   
				}
			}
		};
		
		
		for(Component c : getMenuComponents()) {
			((JMenuItem) c).addActionListener(nasluchWyboruPliku);
		}
		
	}
	
}
