

import java.awt.Color;

import javax.swing.*;


/**
 * Obiekt JMenuOpstions tworzy podmenu "Options" z wyborem rozmiarem i kolorem czcionki oraz kolorem tła.
 * Klasa impementuje również nasłuch do tych opcji.
 * @author Przemysław Sterniński
 *
 */
public class JMenuOptions extends JMenuAbstract {
	
	//Kolory Czcionki i tła, rozmiary czcionki
	private String nazwyKolorow[] = {"Blue", "Yellow", "Orange", "Red", "White", "Black", "Green"};
	private Color listaKolorow[] = {Color.BLUE, Color.YELLOW, Color.ORANGE, Color.RED, Color.WHITE, Color.BLACK, Color.GREEN};
	private int rozmiarCzcionkiOd = 8;
	private int rozmiarCzcionkiDo = 24;
	private int ZmianaRozmiaruCzcionki = 2;
	
	
	public JMenuOptions(EditorView widok){
		  super("Options");
		
		  JMenu subMenuOptionsForeground = utworzDodajSubMenu("Foreground");
		  JMenu subMenuOptionsBackground = utworzDodajSubMenu("Background");
		  JMenu subMenuOptionsFont_Size = utworzDodajSubMenu("Font Size");
		  
		
		  //Rozmiary Czcionki
		  for(int i = rozmiarCzcionkiOd; i <= rozmiarCzcionkiDo; i += ZmianaRozmiaruCzcionki) {
			  JMenuItem elementMenu = utworzElementMenu(subMenuOptionsFont_Size, i + " pts", null, 0, null, i + "");
			  elementMenu.addActionListener(
					  						(e) -> {
				  									widok.getObszarTxt().setFont(widok.getObszarTxt().getFont().deriveFont(Float.valueOf(e.getActionCommand())));
					  						});
		  }
		  
		  
		  for(int i = 0; i < nazwyKolorow.length; i++) {
			  //Dodanie JMenuItem do menu koloru czcionki
			  JMenuItem elementMenu1 = utworzElementMenu(subMenuOptionsForeground, nazwyKolorow[i], new IconKolka(listaKolorow[i]), 0, null, i + "");
			  elementMenu1.addActionListener(
				  							(e) -> {
			  										widok.getObszarTxt().setForeground(listaKolorow[Integer.parseInt(e.getActionCommand())]);
				  							});
		  
		  
			  //Dodanie JMenuItem do menu koloru tła
			  JMenuItem elementMenu2 = utworzElementMenu(subMenuOptionsBackground, nazwyKolorow[i], new IconKolka(listaKolorow[i]), 0, null, i + "");
			  elementMenu2.addActionListener(
					  						(e) -> {
				  									widok.getObszarTxt().setBackground(listaKolorow[Integer.parseInt(e.getActionCommand())]);
			  								});
		  }
	}

}
