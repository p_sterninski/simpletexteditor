
import java.awt.*;

import javax.swing.Icon;


/**
 * Klasa tworzy linię o podanym w konstruktorze kolorze.
 * @author Przemysław Sterniński
 *
 */
class IconLinia implements Icon {

	  private Color color;
	  private int wys = 2;
	  private int sz = 10;

	  public IconLinia(Color c) {
	    color = c;
	  }

	  
	  
	  public void paintIcon(Component c, Graphics g, int x, int y) {
		Color old = g.getColor();
	    g.setColor(color);
	    int sz = c.getWidth()-2*x;
	    g.fillRect(x, y, sz, wys);
	    g.setColor(old);
	  }

	  
	  
	  public int getIconWidth() {
	    return sz;
	  }

	  
	  
	  public int getIconHeight() {
	    return wys;
	  }

	}