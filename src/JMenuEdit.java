

import java.awt.Component;

import javax.swing.*;


/**
 * Obiekt JMenuEdit tworzy podmenu "Edit" z możliwością szybkiego wyboru (skróty klawiszowe) 
 * wybranego adresu mailowego (służbowego, szkolnego i domowego).
 * Klasa impementuje również nasłuch do tych opcji.
 * @author Przemysław Sterniński
 *
 */
public class JMenuEdit extends JMenuAbstract {
	
	public JMenuEdit(EditorView widok){
		super("Edit");
		
		JMenu subMenuEditAdresy = utworzDodajSubMenu("Adresy");
		
		utworzElementMenu(subMenuEditAdresy, "Praca", null, 'P', "control shift P", "adres@praca.pl");
		utworzElementMenu(subMenuEditAdresy,"Szkoła", null, 'S', "control shift S", "adres@szkola.pl");
		utworzElementMenu(subMenuEditAdresy, "Dom", null, 'D', "control shift D", "adres@dom.pl");
		
		for(Component c : subMenuEditAdresy.getMenuComponents()) {
			  ((JMenuItem) c).addActionListener(
					  							(e) -> {
				  										widok.getObszarTxt().insert(e.getActionCommand(), widok.getObszarTxt().getCaretPosition());
			  									});	 
		}
	}
	
}
