
import javax.swing.SwingUtilities;


/**
 * Prosty edytor tekstowy z opcjami w menu rozwijalnym.
 * Opcja "File", pozwala zapisać i otworzyć plik oraz wyjść z programu.
 * Opcja "Edit" dodaje mail służbowy, szkolny lub domowy w miejsce ustawienia kursora.
 * Opcja "Options" pozwala wybrć kolor i rozmiar czcionki oraz kolor tła.
 * Do opcji "File" i "Edit" dodano mnemoniki i akceleratory.
 * @author Przemysław Sterniński
 *
 */
public class Editor {

  public static void main(String[] args) {
	  SwingUtilities.invokeLater(new Runnable() {

		@Override
		public void run() {
			new EditorView();
		}
	  });
  }
}