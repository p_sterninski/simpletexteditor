
import java.awt.*;

import javax.swing.Icon;


/**
 * Klasa tworzy koło o podanym w konstruktorze kolorze.
 * @author Przemysław Sterniński
 *
 */
class IconKolka implements Icon {

	  private Color color;
	  private int w = 15;

	  public IconKolka(Color c) {
	    color = c;
	  }

	  public void paintIcon(Component c, Graphics g, int x, int y) {
		Color old = g.getColor();
	    g.setColor(color);
	    int p = w / 4, d = w / 2;
	    g.fillOval(x+p, y+p, d, d);
	    g.setColor(old);
	  }

	  
	  
	  public int getIconWidth() {
	    return w;
	  }

	  
	  
	  public int getIconHeight() {
	    return w;
	  }

	}
