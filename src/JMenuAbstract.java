import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;


/**
 * Klasa absrakcyjna z dwama metodami utworzDodajSubMenu i utworzElementMenu. 
 * @author Przemysław Sterniński
 *
 */
public abstract class JMenuAbstract
	extends JMenu{
	
	public JMenuAbstract(String txt) {
		super(txt);
	}
	
	
	/**
	 * Metoda tworzy, zwraca i dodaje obiekt JMenuItem do podanego w parametrze obiektu JMenu
	 * @param subMenu obiekt do którego ma zostać dodany obiekt JMenuItem
	 * @param opis tekst widoczny na JMenuItem
	 * @param icon ikona widoczna na JMenuItem
	 * @param mnemo litera będąca skrótem mnemonicznym
	 * @param accel skrót klwiszowy, będący parametrem metody KeyStroke.getKeyStroke();
	 * @param actionCmd 
	 * @return obiekt JMenuItem, z właściwościami zgodnymi z podanymi paramentrami
	 */
	protected JMenuItem utworzElementMenu(JMenu subMenu, String opis, Icon icon, int mnemo, String accel, String actionCmd){
		  JMenuItem menuItem = new JMenuItem(opis, icon);
		  menuItem.setBorder(BorderFactory.createRaisedBevelBorder());
		  menuItem.setMnemonic(mnemo);
		  menuItem.setAccelerator(KeyStroke.getKeyStroke(accel));
		  if(actionCmd != null) {
			  menuItem.setActionCommand(actionCmd);
		  }
		  subMenu.add(menuItem);
		  return menuItem;
	 }
	  
	
	/**
	 * Metoda tworzy, zwraca i dodaje obiekt JMenu do obiektu wywołującego tą metodę 
	 * @param NazwaSubMenu tekst widoczny na twrzonym obiekcie JMenu
	 * @return obiekt JMenu
	 */
	protected JMenu utworzDodajSubMenu(String NazwaSubMenu){
			  JMenu subMenu = new JMenu(NazwaSubMenu);
			  subMenu.setBorder(BorderFactory.createRaisedBevelBorder());
			  this.add(subMenu);
			  return subMenu;
		  
	}
	
}
