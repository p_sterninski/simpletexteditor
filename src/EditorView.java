import java.awt.*;
import javax.swing.*;

/**
 * Klasa opisująca widok edytora tekstowego
 * @author Przemysław Sterniński
 *
 */
public class EditorView extends JFrame {
	private final JTextArea obszarTxt;
	private final JMenuBar pasekMenu;
	private String nazwaProgramu = "Prosty edytor - ";


	public EditorView(){
		setTitle(nazwaProgramu + "bez tytułu");
		  
		obszarTxt = new JTextArea(30, 75);
		obszarTxt.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));

		JScrollPane scroll = new JScrollPane(obszarTxt);  
		  
		pasekMenu = new JMenuBar();
		pasekMenu.add(new JMenuFile(this));
		pasekMenu.add(new JMenuEdit(this));
		pasekMenu.add(new JMenuOptions(this));
		  
		setJMenuBar(pasekMenu);
		add(scroll, BorderLayout.CENTER);
		  
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);	  
	}
	

	
	public JTextArea getObszarTxt() {
		return obszarTxt;
	}


	
	public JMenuBar getPasekMenu() {
		return pasekMenu;
	}



	public String getNazwaProgramu() {
		return nazwaProgramu;
	}



	public void setNazwaProgramu(String nazwaProgramu) {
		this.nazwaProgramu = nazwaProgramu;
	}
	
	
	
  
}